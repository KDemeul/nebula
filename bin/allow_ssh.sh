############################################################
# This script uses gitlab runner variables to:             #
# - install and update a ssh agent                         #
# - de-activate ssh scrit host key checking                #
# - add the gitlab-secret GITLAB_RSA to the ssh agent #
############################################################
if hash ssh-agent 2>/dev/null; then
	echo "ssh-agent already installed"
else
	echo "installing ssh-agent"
	apt-get update -y
	apt-get install openssh-client -y
fi
mkdir -p ~/.ssh
eval $(ssh-agent -s)
# disable host key checking (NOTE: makes you susceptible to man-in-the-middle attacks)
# WARNING: use only in docker container, if you use it with shell you will overwrite your user's ssh config
if [[ -f /.dockerenv ]]; then
	echo -e "Host *\n\tStrictHostKeyChecking no\n\n" > ~/.ssh/config
fi
# add ssh key stored in GITLAB_RSA variable to the agent store
ssh-add <(echo "$GITLAB_RSA")
