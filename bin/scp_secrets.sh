################################################
# This script uses gitlab runner variables to: #
# create the secret files on the host machine  #
################################################
if [ "$#" -ne 1 ]; then
    echo -e "Usage:\nscp_secrets.sh HOST\nwith:\n\t- HOST being user@host"
else
    echo $SECRET_KEY | ssh $1 "cat > ~/secret_key.txt"
fi