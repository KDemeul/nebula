#!/bin/bash
set -e

# Migrate the database if required
python3 /app/manage.py migrate --noinput -v 0

# Collect the static files
python3 /app/manage.py collectstatic --noinput --clear -v 0

# Launch supervisord
/usr/bin/supervisord
